const CAMPOS_FORM_NUEVO_PRODUCTO = ['Descripcion', 'Imagen', 'Categoria', 'Precio']
const CATEGORIAS = ['Equipos y Notebooks', 'Consolas de video juego', 'Mothers y Combos', 'Procesadores y Coolers cpus', 'Placa de video y Otras', 'Almacenamiento', 'Memorias RAM']

let opc_nuevoproducto = document.getElementById('item-nuevo-producto')
let opc_inicio = document.getElementById('item-inicio')
let btn_aceptar = ""
let btn_cancelar = ""

opc_nuevoproducto.addEventListener('click', mostrarFormularioNuevoProducto)
opc_inicio.addEventListener('click', ocultarFormularioNuevoProducto)

function mostrarFormularioNuevoProducto(){
    cambiarFoco('item-nuevo-producto')

    if(document.getElementsByTagName('form')[0]==null){
        let div_img = document.createElement('div')
        div_img.classList.add('w-25', 'h-25', 'm-5')
        div_img.id = 'div-img'
        let img = document.createElement('img')
        img.classList.add('w-100', 'h-100')
        img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABBVBMVEX///8AAAD8/Pzm5+j0PQzfDgfp6uv6+vrx8vPq6+z39/f2QQzj5OXu7/D/Qg3u7u7jDgdlZWXe3t42NjbDw8OioqLX19ftDwdYWFg7OzsXFxdfX1/R0dFMTEysrKweHh4PDw+FhYW9vb2UlJQqKipERER7e3uOjo61tbXyEAhtbW2rq6tJSUntLwrKysomJiafn58qAwEOAQCGCASkDAawLglgBgPQDgclBQG4DAbVOAs6BAE2DgPAMgloGwXnPAxRFAR9IAccAgFbFwSaKQiTCgVwBwR8CARxHAXCDgY1BAGECgREBAL4LgtSCAM6AAFGEgOOJAikLQirEQZJEwQ7DwMiCQEh7Q61AAAUyElEQVR4nO1dCVfbxhbWgqXRICHvNth4ZbENGEpoKKRNXtuQhKbpy3t97f//KW8WjTSrMZHBpkff6Tk9wbY8n+bu945sWQUKFChQoECBAgUKFChQoECBAgUKFChQoECBAhTd3smkue5FPBn8UaVvY/Rnveq6F/MUqJ/aGfqdXuise0WrRTizJQw6DW/dq1od4mOZH8X++J+hlH5jT08Q42BYj9e9wLyoH5r5EdRmI+8FK2VVUsDzvo7koNJ4oeZVVsBZE8Jqr63fymEdrnu9jwWUFPB0bkVBEADoTo4OtFvZnlRfkLw6811x+WNoORg+BFHk1oda/dzbP+6+ENMjK2C7SflhWE4UxhB0DU6kNuttPkk4lORvZGUEMcfIdcPAOtJTRNgd1zdZXuGJ5AGHQOCHAF03gngPf/vjOwPJQaVX9ddNRY/uvrjU/bklE7SAC5robV9/OmuV7z//20Byb3843zyBdWTtunBkfsjaBKCBXvrP9s729taXL637V7+Z5LU/O3HXzUlALG3gLPQjhaBjYUP79XZniwCxbLWu7343kbRrx3WwbmIMQHQRtTlSuNCV+XnIxFy+T/hRklvl8rR09foXE8lB+6S5ERGBYB0Hwxi4CGHMy6nlT2q2/estTxBzLJfKrVbpysjRHpxugFbG/IpOu9BzKTh+VthBr32/IxFEKCGUp6+MDDHOZ731ppUT7o43LMbPdWHKj7iIt1sqv63tM8Swdf0D+fAilofH8/X5yotsHRdW6GZgARsW0B9/0vBLxPTqEn20PYdgfrwo6zqfjcL1MOT28LALg4xhAFE4Ckko97NuA8kWtsof0Ov9Ht0h0B3umhhikp2T6vObnlgIRocezCiGoevjUMewgciYlqb32Cm2AysT6fmRNp9k2B93n5fgibyCYZjaGheGuNr2XmNh6A6WyQbuTYT4B6coC3fS3nvGtLKpU5xhlchqGAGsov+SXURGsHWDTUwlVuIfy4+6433NpVP0Z6PnsK/xWPjWPz4kEUofcYwC2ESJ/S//3UbRi0QN/wWFNKWP+L0NJYAlMawHQLPRXmRfD2aTp/aU9Rr/hb/dTKetP5nvHs5JJvjruy9IFEsSymdnW2dTsoHtqoYfoRgiS+V0tXWBFIPZ5AkzEU9Md6+mLezaWld8YvR5KpNjaJVe4zec6DaQIsKCviCbZKg80U46F4LBe/sOJwxkf1pXP6R/vpkq+0c3cXpHfGBo5IeAXCvsnpPr1CqdBaZnr/IEhqcpmIEfv99JAxS8PXcpx4/I32kIXv+BX2zARQSd0AM9cpHzRuw48UJPeTpZLT8opoM/v2PWMiHQKn1OX/xckvexPL3CL+waNDBVRBBQPUA7bWEfgj2lmWNllds4EgSUd+eZmhFHx/ZR4NgqEYt7YdZAzM8BzogaUs7Wou+uD03hQH9lIV21Ilz4P1k8lkgp5XGdveXyw3Uqq0hN8Z/2F2+gH8XekHx2vym+Ee1k/Uiw4SlqqykLOA3hHn7l3fk2R/D+Df+2lGPrGpvQwdjXErRw7cMHsRfCpO6jlrPwRsb1mc6LzFZBsCoGGu+3eYLpFiJTSV7+9+vMrCJZbU2nJI047Mr7gm8dqRoHHgpmQ9dq0A9N9KKMSY7UPsFgBX2QhnDFT++EeOwsk1Ciaf+7/fJl+mdWiHl99/lv/P9UAxkvEFNimBsGcDvkAx3PLMros8FE1BfbHucmKETZX8WEId3Bcotk7D/QjP5L+U9BXnGdmAok3rIsXeYAJ9QJ6up1PMcoAmGjw1/7IC9Bl48S32/pCbaor3ub7C/OH+74MOeILNvXkyNJJXUJtflCfgjAdb0oqnKzAoO8DLk4+y8pYWAEyy3qCb/nDey0fJd9creOrgTC0EAQJNnKTE04ZJBENIT17NqneRlmVkbM+FC8Vk4szD15+RO/v9tl7B65WtPByAcGgp6flEQMJkaQUkA/wSnjSV6GzA39fYuC0CQpwv87KzNfTtz833JGT197dZntYw9E2g30qIF8wFsmDLGcB3yFYS+3LWX2+WpaKqMEKEHmy29IlV6ysMxNlqfXWZhj748ilSMYUS93rDpBnZAiOYiqvM/o5CWYmtLvSi01mmZx2vdKRr9dZl7yT5vnaIkcA9adU3s6pi2EXSH8yF2/cTJLcyPnfUk0bb/dVksWaajTuuHXg2yOn3EMYZWutrLUBiJfEaZKmyC3N+zyI1yvxKSoVfoX/uuP6gYyPSQE5eL9fp3powcSAWksdoIpQRAGUgo+y1kxlhu8r0sZxXJSvFA0kHck5RZ1GZd//cVdpkM4eiCZEKt1l5JQx4IhaIrpYicnwbkS6X53zyS1VSYFiUv9BiYEp6U/qBjf7uzc/syvDKld0KCxxGxxSpzBD52RuJyjfAQ9IQA8TbKXq2mygeRfP2s0MCXI1PQX6kh23n3irjdg7fElnCDdQd8FUi48ysXP6fEd+tMRSr7p9T+WW+UkSDNWtc+omr6mG5iK8c7tW1kokKXwIYTOgzQtCMJT4ZO1fMOATSG4PY6J3lCz+vt9mfpxU1uCxjrTGxqY/pePdHZu/ycxPLFcHOrEQJ88pgQBmJ8LH+zkS32Fou9h16G2z+HaMfaloa+0lUjoR/KuvyQ7tLPzkzSscAFobzUMgNmiWsCXGgnDXPy6wt3qWSygREl4anu+4g2UKttZLDe9pz7ivXoXdr7cSLMK45j2dRBJaCAJIikrzKWCnqDPFVzdjFlWEIcsYLpGVMokjEvXnsZySbKo5CJsl1tXIseDCzfpXYVupJK0IKyKnZLdXCo44l3EeY+YAMsHHuUYgMTjmgvbLNs3GVrcnCnfXQpLrl2kUTkhKW4gEOt8diVPe0YMGbIGEfL+SfbqJ2XTD7qqL+/kDYZ2i2aPpY+i2PUbEetBIoEJAPTpF8PI86W5nVwqKMR8tbrY4WMU69RTv7nWhOKl6TWtX3zSG9qEYkksrhIMLoDQZ6VCEwJPrIMN8hS6xar9UEq4U4MDkgT7lxuFYjlJ7H/QhzrcJmJzK0oquqWNACjJ1Vysk57mSAeh4CJqSjpjsW8HqdO4kySVFkZxe20hQVqhK7dsBbuNSOQIJSdRyTEx1RTu1bHqgC06jhDyQwqvy/w2JlGazkeoe9gqieU4dmd7XLUj8KU47fjb+Yk+fl/TALNgSI2p8KWX9ynFJBY3N7gFPWR3w7blEwz9HkzsKpCcxKCeg+CQu9CevmZJv7SL05fzcbfJjO4VldRyK4nS3i8yMZTfVrnMzMxBoxqHYzEesw97AYgiACZibrObpyLD2+N2UxsGkxIQmUKwO4GF4kRmdz9iiq0Wtf6mYDyjh8M65jHtI9rJDfkbjHHaazYnHfFvuTppTe5C+hkCYsdhSIwtyXesIK1ZvrmesjDb5OQTcijGw3FPKqG95LsQR7lWryBfuSK7WxVXo4GOH+ASFxmFTaMAKwAhk6I3tBdzKWrg9nZafsQhHQvqmL7aNa58qA7EizjIV3BK5wwHPYiyNd93nDR5RlEFIEYUVvF92Mu22HcDVxAk7CMSQlnVEUMMCVgL7kisPqGAX38AhaCdr9nbY9eZQBxKoP3ygjiK0H+08YWLRrCHY0O+dYnrQrxh/RWr2Jk+lOMIslq/OpCBYmLzDN9+rtmLIdtCvuzONb7QBrpET6QmNQ7hsjjvduchdtggJRJ6ri1wW87EOC2Uq+jEGNrHXGDIwaPpp1x2tyI8GlJn/uzTlwcZsrTRPjJVnyyrYRoXymNqssioGWsIJr3LsXqUIhTc8q/qHJQAlnWQ4FlPEHOMx/qxhL0cqhhk0q72FTxAepfaoiYR4shlFuKNrvCfEUwqUw9OnKjukSJPXp+ZxGNfIgg98uJQWyhiQswM/eW9OS/OJHRxzYlwrOpmaPLkhfPsMhMhtg8gsbO1rn5RzChZqS+7M1AsT5NxIvlclB4e1LjHXF2mbBP7VW6iGdCi4pGpb8JMrZ/F7a+1ythKit/amF4DEEawK4c5uRiGmXKf+tkGkrr7Qd101yFLiauc/ftdo4zT+2QARS/sKnwcQ8GJyPAoD0M+5xsmtT1IG5HGDXRYKxBWhYVcKl249GSF8V4poMIvxt4529mcal+QRivNHWrmRSVVDS9Jwwf7aQotduGSkTYUeT08hyAxFOU055F+j3NCIxhDGiTKtRqeIN3qiAZu/XEM03zK/sApIx3rtrVlAyOwlMrN3t18BFFMyF2sVyUJ4/kCu5fsIAhJQw+FO5bDyXqqjKy6v6QN5S/OT5Rg5B8mVVyQZoAuQ0QI+pTTcVIUsJqpyaHK2LpOqtunwfL86BZCsZttV3ITtHyxLlnTp/qEiJNIaEScVn+UzYKC1Di8Kk+n6Uzt+BEbiDuFWW09Xc4q5rr5TN8+N2og4kdL/KBLbEuHd3GcKPz2gQ0p7i1vQx2i4GHkynnUap4cwtfbKqRUoVkAjFxqQ2nKb0tTo1LVDuNwOS/P7h+Sf9CValMrUEIK7sYdWJ7r4UQYQIjTfh+3avFcYVpsJ8b2QI3nLKnZPltu0ILxQ4mp07Ml5Otnc+Bd95gFqGEGLiCnNLR5ntXkd6C9tJNA4h/j+oI/lPjl7GcL4CVsrp1CoyaGzmPvGTxAxA8VD5clCIFHkzE5Hm2v8vCIwxWDaoFpGhTSYUnT0RArCqLMElbS4WAHCzqVefJPNgiNyMEoifdDuc6dfyhIQsjV2Ctyrkjh+dTEaAurFKHLndU/TpqskSuKO9JyBM9zs1Yabt1J/HBtaLXgzcRYV7YBNCDfXWAgcQ/Hz6xFBysrDDXXUm4euJAJ5mlVGMCHNl1FFZOqIkpAFqgX6cPBbKIK3Q3LJPE8gkiOq3IWgvWAXLh7AKWFJXH24AEXTvY+qqYzaP26tQzBQC4JHz7NE5e4koY9E+XUnxD67YdmJakCB342xHTh6DgJAE2J32pPNfHgG1ENjiJIho8XH17KGLpRl7tXsdn5EMgZfd6hoEWIOXs96KZlG0AflrT74IEBLUOUYCk9eoGg8hiixpMRFOW0lmxikFSblpqVTD7jCFWIfl1fUSdXh0p37QmMKAf+fg7xhoSQmg1TFCOCFnACXy4Ijh29RUVuXj5HubpTdwbwcUUPelGSjmqOmGuFlFT7Q7VZNvN0XYPQr8t1/JUGalo0+fGB4yo9MjZ42MRQeC43B7M74w7VHTY1yihPlJCA/snRkL8UCc4SJgaLqINtk88u0IPiIyZGcijoycPkqziNtgw68tea66YCPxLPxKwG0SZH7YTzb0NfUMY4VoR5xeeXTYilr12u0uIDnCHTkRT8IZb8WvOs89kOOc8IlKcLPEmgpoXogGsLdjDh7rMUCCaNXN7wclU4+2CeKiOU6gG2ffqMT6MRPNQhpUKPEXLA6R4AET4PmtQ3POa7a8JxZcvj5tAniTI6irrnnEh4HGJ+0G2AluvjoYxQC07s2ES9HBpYDpc4HAEQRL46RPN0gZoWQjLasaJlMjyYDJ9q9FaYXj04qU/ULmjveQmKw252z3RAUrD8CYnzkU5tLfOkBcHTBmo6+IKdqz6QHOAMNnESpk691V3wKOX+Oh4XLWRsbXPgnEgoi4Rm5n5AbHyQx9MHaloI9evh4loLZJWZBSUqx4KG8bWjNT3eU+zWnCzYRS8a0jc9cOBcU/Und289/CxJTu0wMBEEQZKO7D+YflhKMm8Pnt2IchDi/howlMzSMucywZ3VlJOl54vUdBBC8KG2EsFOteGa2oP8MMVQfmbZYL5GhqHgwyYaihE7drV0G81SehN763wuq9Dq2nNlOQ0hO8cwW77LFEE5oMk3LJMTwmJ25Qw2PRi44BllCkBoyfX7df48hBCCS6oYsVy3bxh90+8hwImTGMKtdRPnwlLqfALLyk1LzqulUkryZMGkHq71CcmCnJ5nqpgWyY4e0cnGoL3QSAjh1vqrEI4gp+1EFbODgY+ZBnKyU3CRkJ+t02HIcnpM2iyAHdjfbT5uA9O5VE8oWq5sGuHbIHYVTmAQgHkSp+kOoSzeQhb7BXxfY817aPlid302GrEc4VHjToRgWiwQy/7r/nUW19ZiyTiNRxAyRyMYmpWMdeWCMsWD0X6shDppcyaShi7W6g8pNKfLlquDc7DSDBPID05Za3pB4cmzZnb7kSpoOXF6zGguXSv/455WAHnUZVC10oL3Uj0N6KYElXbTZvyyxVBcVA2yx+h5gfwcBB0/ZmJQPqm0tFc+F/RtgGJu3g8tLgr3NE+04Phxz79SpmNX9IjOVUCS0xNxSiZ0geFJOpbDFczjQKkoVjbn14IkOZXLNmHoAV/dST/mOhtwrhS+VzDCvTIA8UFGM115McAksyeGOFAo0MGeQnBjRJRAmltq6CpT9NkkkPTeQOAKZWSoVkvzPCfhKSCtcG4YA9L03VzcuVH7Tc/U034ExFjr/MF2DYfIU3/5YAN/vbMq6lFHP2arJajMBa10gnt1kDrTJwvH1ThIdZnNJagMojSXElTliYD2yp42vnp4YptzN15m/tdRjWh7Q38vz7KUIyMPtU5xSKca0ZyPdXxaSKmirpkhgD+dwLBpblAEkGyGa+wrEkTBqUJwQ5IJI6QQ/NDUVyQAXXUEY82lwyUwFBes7ytSqJNd5LjepkP69UN7ZHIZoZrO23sb6gZFSCH4nklMLfWHcg821Q1KkPzbofZARRQpc6pPd4pi1XCkKHqs639X1VD7OSacV4SmZCHriiqCpvrbMOsbnPkGSCbk3BN1MVRqovZzjXCvDFJosy8mUhojuoHp7mIos+BcgKqpia5jvDIv5G5NdkjK003orblD+E2QaOwyexoD9cDMwbobhN+EWDKWR1ROYVc1omuaH80NuVsz9qMIwBP1eWQvxs8rGEpMZvPqSI1jNqH/+c1Y+CO36daue5V50H2Y35MeB30GaKeaBeT+RaY1Ay78CV/75QUyKqoL+fU3YAQhN9TR9AyHwbpXtxKYfwu1vdaZytXB0XhAgsrmlrUfCQPFza76Pg6ORlBrLy9ZWgg5HRwcv9BQ24y54BcrLzJXegjdo2Qa73D8j+SH4VcvhseTf4YLLFCgQIECBQoUKFCgQIECBQoUKFCgQIECBTYC/wfkcMYpfUiY3QAAAABJRU5ErkJggg=='
        img.id = 'img'
        div_img.appendChild(img)
        let form = document.createElement('form')
        form.classList.add('w-50')

        for(let i = 0; i < CAMPOS_FORM_NUEVO_PRODUCTO.length; i++){
            let div = document.createElement('div')
            div.classList.add('form-group')

            let label = document.createElement('label')
            let text_label = document.createTextNode(CAMPOS_FORM_NUEVO_PRODUCTO[i])
            label.appendChild(text_label)

            if(CAMPOS_FORM_NUEVO_PRODUCTO[i] == 'Categoria'){
                let select = document.createElement('select')
                select.classList.add('form-control')
                select.id = CAMPOS_FORM_NUEVO_PRODUCTO[i].toLowerCase()
                for(let j = 0; j < CATEGORIAS.length; j++){
                    let opcion = document.createElement('option')
                    opcion.value = CATEGORIAS[j]
                    let text_opcion = document.createTextNode(CATEGORIAS[j])
                    opcion.appendChild(text_opcion)
                    select.appendChild(opcion)
                }
                div.appendChild(label)
                div.appendChild(select)
            }else{
                let input = document.createElement('input')
                input.classList.add('form-control')
                input.type = 'text'
                input.id = CAMPOS_FORM_NUEVO_PRODUCTO[i].toLowerCase()

                div.appendChild(label)
                div.appendChild(input)
            }
            form.appendChild(div)
        }

        let button_aceptar = document.createElement('button')
        button_aceptar.classList.add('btn', 'btn-primary')
        button_aceptar.id = 'aceptar'
        button_aceptar.type = 'button'
        let texto_aceptar = document.createTextNode('Aceptar')
        let button_cancelar = document.createElement('button')
        button_cancelar.classList.add('btn', 'btn-light')
        button_cancelar.id = 'cancelar'
        button_cancelar.type = 'button'
        let texto_cancelar = document.createTextNode('Cancelar')

        button_aceptar.appendChild(texto_aceptar)
        button_cancelar.appendChild(texto_cancelar)

        form.appendChild(button_aceptar)
        form.appendChild(button_cancelar)

        let container = document.getElementById('container')
        container.appendChild(div_img)
        container.appendChild(form)

        btn_aceptar = document.getElementById('aceptar')
        btn_cancelar = document.getElementById('cancelar')
        input_img = document.getElementById('imagen')
        btn_cancelar.addEventListener('click', ocultarFormularioNuevoProducto)
        btn_aceptar.addEventListener('click', agregarProducto)
        input_img.addEventListener('blur', cargarImagen)
    }
}

function ocultarFormularioNuevoProducto(){
    let container = document.getElementById('container')
    let form = document.getElementsByTagName('form')[0]
    let div_img = document.getElementById('div-img')
    container.removeChild(div_img)
    container.removeChild(form)

    cambiarFoco('item-inicio')
}

function cambiarFoco(itemseleccionado){
    let item_activo = document.getElementsByClassName('active')[0]
    item_activo.classList.remove('active')

    let item_seleccionado = document.getElementById(itemseleccionado)
    item_seleccionado.classList.add('active')
}

function limpiarCampos(campos){
    campos.forEach(function(id){
        document.getElementById(id).value = ""
    });
}

function cargarImagen(){
    let parametro = /^(http|https)\:\/\/[a-z0-9\.-]+\.[a-z]{2,4}/gi
    let img = document.getElementById('img')
    src = document.getElementById('imagen').value
    if(src.match(parametro)){
        img.src = src
    }else{
        img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABBVBMVEX///8AAAD8/Pzm5+j0PQzfDgfp6uv6+vrx8vPq6+z39/f2QQzj5OXu7/D/Qg3u7u7jDgdlZWXe3t42NjbDw8OioqLX19ftDwdYWFg7OzsXFxdfX1/R0dFMTEysrKweHh4PDw+FhYW9vb2UlJQqKipERER7e3uOjo61tbXyEAhtbW2rq6tJSUntLwrKysomJiafn58qAwEOAQCGCASkDAawLglgBgPQDgclBQG4DAbVOAs6BAE2DgPAMgloGwXnPAxRFAR9IAccAgFbFwSaKQiTCgVwBwR8CARxHAXCDgY1BAGECgREBAL4LgtSCAM6AAFGEgOOJAikLQirEQZJEwQ7DwMiCQEh7Q61AAAUyElEQVR4nO1dCVfbxhbWgqXRICHvNth4ZbENGEpoKKRNXtuQhKbpy3t97f//KW8WjTSrMZHBpkff6Tk9wbY8n+bu945sWQUKFChQoECBAgUKFChQoECBAgUKFChQoECBAhTd3smkue5FPBn8UaVvY/Rnveq6F/MUqJ/aGfqdXuise0WrRTizJQw6DW/dq1od4mOZH8X++J+hlH5jT08Q42BYj9e9wLyoH5r5EdRmI+8FK2VVUsDzvo7koNJ4oeZVVsBZE8Jqr63fymEdrnu9jwWUFPB0bkVBEADoTo4OtFvZnlRfkLw6811x+WNoORg+BFHk1oda/dzbP+6+ENMjK2C7SflhWE4UxhB0DU6kNuttPkk4lORvZGUEMcfIdcPAOtJTRNgd1zdZXuGJ5AGHQOCHAF03gngPf/vjOwPJQaVX9ddNRY/uvrjU/bklE7SAC5robV9/OmuV7z//20Byb3843zyBdWTtunBkfsjaBKCBXvrP9s729taXL637V7+Z5LU/O3HXzUlALG3gLPQjhaBjYUP79XZniwCxbLWu7343kbRrx3WwbmIMQHQRtTlSuNCV+XnIxFy+T/hRklvl8rR09foXE8lB+6S5ERGBYB0Hwxi4CGHMy6nlT2q2/estTxBzLJfKrVbpysjRHpxugFbG/IpOu9BzKTh+VthBr32/IxFEKCGUp6+MDDHOZ731ppUT7o43LMbPdWHKj7iIt1sqv63tM8Swdf0D+fAilofH8/X5yotsHRdW6GZgARsW0B9/0vBLxPTqEn20PYdgfrwo6zqfjcL1MOT28LALg4xhAFE4Ckko97NuA8kWtsof0Ov9Ht0h0B3umhhikp2T6vObnlgIRocezCiGoevjUMewgciYlqb32Cm2AysT6fmRNp9k2B93n5fgibyCYZjaGheGuNr2XmNh6A6WyQbuTYT4B6coC3fS3nvGtLKpU5xhlchqGAGsov+SXURGsHWDTUwlVuIfy4+6433NpVP0Z6PnsK/xWPjWPz4kEUofcYwC2ESJ/S//3UbRi0QN/wWFNKWP+L0NJYAlMawHQLPRXmRfD2aTp/aU9Rr/hb/dTKetP5nvHs5JJvjruy9IFEsSymdnW2dTsoHtqoYfoRgiS+V0tXWBFIPZ5AkzEU9Md6+mLezaWld8YvR5KpNjaJVe4zec6DaQIsKCviCbZKg80U46F4LBe/sOJwxkf1pXP6R/vpkq+0c3cXpHfGBo5IeAXCvsnpPr1CqdBaZnr/IEhqcpmIEfv99JAxS8PXcpx4/I32kIXv+BX2zARQSd0AM9cpHzRuw48UJPeTpZLT8opoM/v2PWMiHQKn1OX/xckvexPL3CL+waNDBVRBBQPUA7bWEfgj2lmWNllds4EgSUd+eZmhFHx/ZR4NgqEYt7YdZAzM8BzogaUs7Wou+uD03hQH9lIV21Ilz4P1k8lkgp5XGdveXyw3Uqq0hN8Z/2F2+gH8XekHx2vym+Ee1k/Uiw4SlqqykLOA3hHn7l3fk2R/D+Df+2lGPrGpvQwdjXErRw7cMHsRfCpO6jlrPwRsb1mc6LzFZBsCoGGu+3eYLpFiJTSV7+9+vMrCJZbU2nJI047Mr7gm8dqRoHHgpmQ9dq0A9N9KKMSY7UPsFgBX2QhnDFT++EeOwsk1Ciaf+7/fJl+mdWiHl99/lv/P9UAxkvEFNimBsGcDvkAx3PLMros8FE1BfbHucmKETZX8WEId3Bcotk7D/QjP5L+U9BXnGdmAok3rIsXeYAJ9QJ6up1PMcoAmGjw1/7IC9Bl48S32/pCbaor3ub7C/OH+74MOeILNvXkyNJJXUJtflCfgjAdb0oqnKzAoO8DLk4+y8pYWAEyy3qCb/nDey0fJd9creOrgTC0EAQJNnKTE04ZJBENIT17NqneRlmVkbM+FC8Vk4szD15+RO/v9tl7B65WtPByAcGgp6flEQMJkaQUkA/wSnjSV6GzA39fYuC0CQpwv87KzNfTtz833JGT197dZntYw9E2g30qIF8wFsmDLGcB3yFYS+3LWX2+WpaKqMEKEHmy29IlV6ysMxNlqfXWZhj748ilSMYUS93rDpBnZAiOYiqvM/o5CWYmtLvSi01mmZx2vdKRr9dZl7yT5vnaIkcA9adU3s6pi2EXSH8yF2/cTJLcyPnfUk0bb/dVksWaajTuuHXg2yOn3EMYZWutrLUBiJfEaZKmyC3N+zyI1yvxKSoVfoX/uuP6gYyPSQE5eL9fp3powcSAWksdoIpQRAGUgo+y1kxlhu8r0sZxXJSvFA0kHck5RZ1GZd//cVdpkM4eiCZEKt1l5JQx4IhaIrpYicnwbkS6X53zyS1VSYFiUv9BiYEp6U/qBjf7uzc/syvDKld0KCxxGxxSpzBD52RuJyjfAQ9IQA8TbKXq2mygeRfP2s0MCXI1PQX6kh23n3irjdg7fElnCDdQd8FUi48ysXP6fEd+tMRSr7p9T+WW+UkSDNWtc+omr6mG5iK8c7tW1kokKXwIYTOgzQtCMJT4ZO1fMOATSG4PY6J3lCz+vt9mfpxU1uCxjrTGxqY/pePdHZu/ycxPLFcHOrEQJ88pgQBmJ8LH+zkS32Fou9h16G2z+HaMfaloa+0lUjoR/KuvyQ7tLPzkzSscAFobzUMgNmiWsCXGgnDXPy6wt3qWSygREl4anu+4g2UKttZLDe9pz7ivXoXdr7cSLMK45j2dRBJaCAJIikrzKWCnqDPFVzdjFlWEIcsYLpGVMokjEvXnsZySbKo5CJsl1tXIseDCzfpXYVupJK0IKyKnZLdXCo44l3EeY+YAMsHHuUYgMTjmgvbLNs3GVrcnCnfXQpLrl2kUTkhKW4gEOt8diVPe0YMGbIGEfL+SfbqJ2XTD7qqL+/kDYZ2i2aPpY+i2PUbEetBIoEJAPTpF8PI86W5nVwqKMR8tbrY4WMU69RTv7nWhOKl6TWtX3zSG9qEYkksrhIMLoDQZ6VCEwJPrIMN8hS6xar9UEq4U4MDkgT7lxuFYjlJ7H/QhzrcJmJzK0oquqWNACjJ1Vysk57mSAeh4CJqSjpjsW8HqdO4kySVFkZxe20hQVqhK7dsBbuNSOQIJSdRyTEx1RTu1bHqgC06jhDyQwqvy/w2JlGazkeoe9gqieU4dmd7XLUj8KU47fjb+Yk+fl/TALNgSI2p8KWX9ynFJBY3N7gFPWR3w7blEwz9HkzsKpCcxKCeg+CQu9CevmZJv7SL05fzcbfJjO4VldRyK4nS3i8yMZTfVrnMzMxBoxqHYzEesw97AYgiACZibrObpyLD2+N2UxsGkxIQmUKwO4GF4kRmdz9iiq0Wtf6mYDyjh8M65jHtI9rJDfkbjHHaazYnHfFvuTppTe5C+hkCYsdhSIwtyXesIK1ZvrmesjDb5OQTcijGw3FPKqG95LsQR7lWryBfuSK7WxVXo4GOH+ASFxmFTaMAKwAhk6I3tBdzKWrg9nZafsQhHQvqmL7aNa58qA7EizjIV3BK5wwHPYiyNd93nDR5RlEFIEYUVvF92Mu22HcDVxAk7CMSQlnVEUMMCVgL7kisPqGAX38AhaCdr9nbY9eZQBxKoP3ygjiK0H+08YWLRrCHY0O+dYnrQrxh/RWr2Jk+lOMIslq/OpCBYmLzDN9+rtmLIdtCvuzONb7QBrpET6QmNQ7hsjjvduchdtggJRJ6ri1wW87EOC2Uq+jEGNrHXGDIwaPpp1x2tyI8GlJn/uzTlwcZsrTRPjJVnyyrYRoXymNqssioGWsIJr3LsXqUIhTc8q/qHJQAlnWQ4FlPEHOMx/qxhL0cqhhk0q72FTxAepfaoiYR4shlFuKNrvCfEUwqUw9OnKjukSJPXp+ZxGNfIgg98uJQWyhiQswM/eW9OS/OJHRxzYlwrOpmaPLkhfPsMhMhtg8gsbO1rn5RzChZqS+7M1AsT5NxIvlclB4e1LjHXF2mbBP7VW6iGdCi4pGpb8JMrZ/F7a+1ythKit/amF4DEEawK4c5uRiGmXKf+tkGkrr7Qd101yFLiauc/ftdo4zT+2QARS/sKnwcQ8GJyPAoD0M+5xsmtT1IG5HGDXRYKxBWhYVcKl249GSF8V4poMIvxt4529mcal+QRivNHWrmRSVVDS9Jwwf7aQotduGSkTYUeT08hyAxFOU055F+j3NCIxhDGiTKtRqeIN3qiAZu/XEM03zK/sApIx3rtrVlAyOwlMrN3t18BFFMyF2sVyUJ4/kCu5fsIAhJQw+FO5bDyXqqjKy6v6QN5S/OT5Rg5B8mVVyQZoAuQ0QI+pTTcVIUsJqpyaHK2LpOqtunwfL86BZCsZttV3ITtHyxLlnTp/qEiJNIaEScVn+UzYKC1Di8Kk+n6Uzt+BEbiDuFWW09Xc4q5rr5TN8+N2og4kdL/KBLbEuHd3GcKPz2gQ0p7i1vQx2i4GHkynnUap4cwtfbKqRUoVkAjFxqQ2nKb0tTo1LVDuNwOS/P7h+Sf9CValMrUEIK7sYdWJ7r4UQYQIjTfh+3avFcYVpsJ8b2QI3nLKnZPltu0ILxQ4mp07Ml5Otnc+Bd95gFqGEGLiCnNLR5ntXkd6C9tJNA4h/j+oI/lPjl7GcL4CVsrp1CoyaGzmPvGTxAxA8VD5clCIFHkzE5Hm2v8vCIwxWDaoFpGhTSYUnT0RArCqLMElbS4WAHCzqVefJPNgiNyMEoifdDuc6dfyhIQsjV2Ctyrkjh+dTEaAurFKHLndU/TpqskSuKO9JyBM9zs1Yabt1J/HBtaLXgzcRYV7YBNCDfXWAgcQ/Hz6xFBysrDDXXUm4euJAJ5mlVGMCHNl1FFZOqIkpAFqgX6cPBbKIK3Q3LJPE8gkiOq3IWgvWAXLh7AKWFJXH24AEXTvY+qqYzaP26tQzBQC4JHz7NE5e4koY9E+XUnxD67YdmJakCB342xHTh6DgJAE2J32pPNfHgG1ENjiJIho8XH17KGLpRl7tXsdn5EMgZfd6hoEWIOXs96KZlG0AflrT74IEBLUOUYCk9eoGg8hiixpMRFOW0lmxikFSblpqVTD7jCFWIfl1fUSdXh0p37QmMKAf+fg7xhoSQmg1TFCOCFnACXy4Ijh29RUVuXj5HubpTdwbwcUUPelGSjmqOmGuFlFT7Q7VZNvN0XYPQr8t1/JUGalo0+fGB4yo9MjZ42MRQeC43B7M74w7VHTY1yihPlJCA/snRkL8UCc4SJgaLqINtk88u0IPiIyZGcijoycPkqziNtgw68tea66YCPxLPxKwG0SZH7YTzb0NfUMY4VoR5xeeXTYilr12u0uIDnCHTkRT8IZb8WvOs89kOOc8IlKcLPEmgpoXogGsLdjDh7rMUCCaNXN7wclU4+2CeKiOU6gG2ffqMT6MRPNQhpUKPEXLA6R4AET4PmtQ3POa7a8JxZcvj5tAniTI6irrnnEh4HGJ+0G2AluvjoYxQC07s2ES9HBpYDpc4HAEQRL46RPN0gZoWQjLasaJlMjyYDJ9q9FaYXj04qU/ULmjveQmKw252z3RAUrD8CYnzkU5tLfOkBcHTBmo6+IKdqz6QHOAMNnESpk691V3wKOX+Oh4XLWRsbXPgnEgoi4Rm5n5AbHyQx9MHaloI9evh4loLZJWZBSUqx4KG8bWjNT3eU+zWnCzYRS8a0jc9cOBcU/Und289/CxJTu0wMBEEQZKO7D+YflhKMm8Pnt2IchDi/howlMzSMucywZ3VlJOl54vUdBBC8KG2EsFOteGa2oP8MMVQfmbZYL5GhqHgwyYaihE7drV0G81SehN763wuq9Dq2nNlOQ0hO8cwW77LFEE5oMk3LJMTwmJ25Qw2PRi44BllCkBoyfX7df48hBCCS6oYsVy3bxh90+8hwImTGMKtdRPnwlLqfALLyk1LzqulUkryZMGkHq71CcmCnJ5nqpgWyY4e0cnGoL3QSAjh1vqrEI4gp+1EFbODgY+ZBnKyU3CRkJ+t02HIcnpM2iyAHdjfbT5uA9O5VE8oWq5sGuHbIHYVTmAQgHkSp+kOoSzeQhb7BXxfY817aPlid302GrEc4VHjToRgWiwQy/7r/nUW19ZiyTiNRxAyRyMYmpWMdeWCMsWD0X6shDppcyaShi7W6g8pNKfLlquDc7DSDBPID05Za3pB4cmzZnb7kSpoOXF6zGguXSv/455WAHnUZVC10oL3Uj0N6KYElXbTZvyyxVBcVA2yx+h5gfwcBB0/ZmJQPqm0tFc+F/RtgGJu3g8tLgr3NE+04Phxz79SpmNX9IjOVUCS0xNxSiZ0geFJOpbDFczjQKkoVjbn14IkOZXLNmHoAV/dST/mOhtwrhS+VzDCvTIA8UFGM115McAksyeGOFAo0MGeQnBjRJRAmltq6CpT9NkkkPTeQOAKZWSoVkvzPCfhKSCtcG4YA9L03VzcuVH7Tc/U034ExFjr/MF2DYfIU3/5YAN/vbMq6lFHP2arJajMBa10gnt1kDrTJwvH1ThIdZnNJagMojSXElTliYD2yp42vnp4YptzN15m/tdRjWh7Q38vz7KUIyMPtU5xSKca0ZyPdXxaSKmirpkhgD+dwLBpblAEkGyGa+wrEkTBqUJwQ5IJI6QQ/NDUVyQAXXUEY82lwyUwFBes7ytSqJNd5LjepkP69UN7ZHIZoZrO23sb6gZFSCH4nklMLfWHcg821Q1KkPzbofZARRQpc6pPd4pi1XCkKHqs639X1VD7OSacV4SmZCHriiqCpvrbMOsbnPkGSCbk3BN1MVRqovZzjXCvDFJosy8mUhojuoHp7mIos+BcgKqpia5jvDIv5G5NdkjK003orblD+E2QaOwyexoD9cDMwbobhN+EWDKWR1ROYVc1omuaH80NuVsz9qMIwBP1eWQvxs8rGEpMZvPqSI1jNqH/+c1Y+CO36daue5V50H2Y35MeB30GaKeaBeT+RaY1Ay78CV/75QUyKqoL+fU3YAQhN9TR9AyHwbpXtxKYfwu1vdaZytXB0XhAgsrmlrUfCQPFza76Pg6ORlBrLy9ZWgg5HRwcv9BQ24y54BcrLzJXegjdo2Qa73D8j+SH4VcvhseTf4YLLFCgQIECBQoUKFCgQIECBQoUKFCgQIECBTYC/wfkcMYpfUiY3QAAAABJRU5ErkJggg=='
    }
}